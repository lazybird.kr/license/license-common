package config

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"time"
)

type DBConfig struct {
	Hosts            string `mapstructure:"hosts"`
	Database         string `mapstructure:"database"`
	Username         string `mapstructure:"username"`
	Password         string `mapstructure:"password"`
	UserCollection   string `mapstructure:"user_collection"`
	UsageCollection  string `mapstructure:"usage_collection"`
	KeyCollection    string `mapstructure:"key_collection"`
	PolicyCollection string `mapstructure:"policy_collection"`
}

type HostsConfig struct {
	HttpHosts string `mapstructure:"http_hosts"`
	GrpcHosts string `mapstructure:"grpc_hosts"`
	Hosts     string `mapstructure:"hosts"`
}

type ServerConfig struct {
	Control HostsConfig `mapstructure:"control"`
	Usage   HostsConfig `mapstructure:"usage"`
	Key     HostsConfig `mapstructure:"key"`
	Polciy  HostsConfig `mapstructure:"policy"`
}

type ClientConfig struct {
	Control HostsConfig `masteructure:"control"`
	Usage   HostsConfig `mapstructure:"usage"`
	Key     HostsConfig `mapstructure:"key"`
	Policy  HostsConfig `mapstructure:"policy"`
}

type TimeConfig struct {
	Format string `mapstructure:"format"`
}

func LoadConfig(path ...string) (err error) {
	env := os.Getenv("LAZYBIRD_LICENSE")
	configPath := env + "/license-common/config.json"
	fmt.Fprintf(os.Stdout, "configFilePath: %v\n", configPath)
	if len(path) > 0 {
		configPath = path[0]
	}
	viper.SetConfigFile(configPath)
	if err = viper.ReadInConfig(); err != nil {
		return
	}

	return
}

var mongoConfig DBConfig
var serverHostConfig ServerConfig
var clientHostConfig ClientConfig
var timeConfig TimeConfig

func GetConfig() (err error) {
	viper.GetStringMap("db")
	err = viper.UnmarshalKey("db", &mongoConfig)
	viper.GetStringMap("server")
	err = viper.UnmarshalKey("server", &serverHostConfig)
	viper.GetStringMap("client")
	err = viper.UnmarshalKey("client", &clientHostConfig)
	viper.GetStringMap("time")
	err = viper.UnmarshalKey("time", &timeConfig)

	return
}

func GetDBConfig() (config DBConfig) {
	return mongoConfig
}

func GetServerHostConfig() (config ServerConfig) {
	return serverHostConfig
}

func GetClientHostConfig() (config ClientConfig) {
	return clientHostConfig
}

func GetDefaultTimeFormat() string {
	return time.RFC3339
}

func GetTimeFormat() string {
	timeFormat := timeConfig.Format
	if len(timeFormat) == 0 {
		timeFormat = GetDefaultTimeFormat()
	}
	return timeFormat
}
