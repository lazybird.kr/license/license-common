package config

import (
	"fmt"
	"os"
	"testing"
)

func TestLoadConfig(t *testing.T) {
	env := os.Getenv("LAZYBIRD_LICENSE")
	path := env + "/license-common/docker_config.json"
	if err := LoadConfig(path); err != nil {
		t.Errorf("LoadConfig=%v", err)
	}
}

func TestGetConfig(t *testing.T) {
	if err := GetConfig(); err != nil {
		t.Errorf("GetConfig=%v\n", err)
	} else {
		fmt.Fprintf(os.Stdout, "DBConfig: %v\n", GetDBConfig())
		fmt.Fprintf(os.Stdout, "ServerHostConfig: %v\n", GetServerHostConfig())
	}
}
